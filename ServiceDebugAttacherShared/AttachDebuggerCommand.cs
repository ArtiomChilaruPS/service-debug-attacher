﻿using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;
using System;
using System.ComponentModel.Design;
using System.IO;
using System.Linq;
using Task = System.Threading.Tasks.Task;

namespace ServiceDebugAttacher
{
    /// <summary>
    /// Command handler
    /// </summary>
    internal sealed class AttachDebuggerCommand
    {
        /// <summary>
        /// Command ID.
        /// </summary>
        public const int CommandId = 0x0100;

        /// <summary>
        /// Command menu group (command set GUID).
        /// </summary>
        public static readonly Guid CommandSet = new Guid("5ebd4bd5-9ddf-43c7-bf85-1c0d872a622e");

        /// <summary>
        /// VS Package that provides this command, not null.
        /// </summary>
        private readonly AsyncPackage _package;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttachDebuggerCommand"/> class.
        /// Adds our command handlers for menu (commands must exist in the command table file)
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        /// <param name="commandService">Command service to add command to, not null.</param>
        private AttachDebuggerCommand(AsyncPackage package, OleMenuCommandService commandService)
        {
            _package = package ?? throw new ArgumentNullException(nameof(package));
            commandService = commandService ?? throw new ArgumentNullException(nameof(commandService));

            var menuCommandID = new CommandID(CommandSet, CommandId);
            var menuItem = new MenuCommand(Execute, menuCommandID);
            commandService.AddCommand(menuItem);
        }

        /// <summary>
        /// Gets the instance of the command.
        /// </summary>
        public static AttachDebuggerCommand Instance { get; private set; }

        /// <summary>
        /// Gets the service provider from the owner package.
        /// </summary>
        private Microsoft.VisualStudio.Shell.IAsyncServiceProvider ServiceProvider => _package;

        /// <summary>
        /// Initializes the singleton instance of the command.
        /// </summary>
        /// <param name="package">Owner package, not null.</param>
        public static async Task InitializeAsync(AsyncPackage package)
        {
            // Switch to the main thread - the call to AddCommand in AttachDebuggerCommand's constructor requires
            // the UI thread.
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync(package.DisposalToken);

            OleMenuCommandService commandService = await package.GetServiceAsync(typeof(IMenuCommandService)) as OleMenuCommandService;
            Instance = new AttachDebuggerCommand(package, commandService);
        }

        private void ShowErrorMessage(string message)
        {
            VsShellUtilities.ShowMessageBox(
                _package,
                message,
                "Service Debug Attacher",
                OLEMSGICON.OLEMSGICON_CRITICAL,
                OLEMSGBUTTON.OLEMSGBUTTON_OK,
                OLEMSGDEFBUTTON.OLEMSGDEFBUTTON_FIRST);
        }

        /// <summary>
        /// This function is the callback used to execute the command when the menu item is clicked.
        /// See the constructor to see how the menu item is associated with this function using
        /// OleMenuCommandService service and MenuCommand class.
        /// </summary>
        /// <param name="sender">Event sender.</param>
        /// <param name="e">Event args.</param>
        private void Execute(object sender, EventArgs e) => _ = ExecuteAsync();

        private async Task ExecuteAsync()
        {
            await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            if (!(await ServiceProvider.GetServiceAsync(typeof(SDTE)) is DTE2 dte))
            {
                ShowErrorMessage("Could not initialise DTE object");
                return;
            }

            if (!(dte.Debugger is Debugger2 dteDebugger))
            {
                ShowErrorMessage("Could not initialise Debugger object");
                return;
            }

            var solutionRoot = Path.GetDirectoryName(dte.Solution.FileName);

            var startupProjects = (dte.Solution.SolutionBuild.StartupProjects as Array).OfType<string>();
            if (startupProjects == null || startupProjects.Count() < 1)
            {
                ShowErrorMessage("No startup project selected");
                return;
            }

            var processName = Path.GetFileNameWithoutExtension(startupProjects.First()) + ".exe";

            try
            {
                var allProcesses = dteDebugger.LocalProcesses.OfType<Process2>();
                var goodProcess = allProcesses
                    .Where(p =>
                        Path.IsPathRooted(p.Name) &&
                        Path.GetFullPath(p.Name).StartsWith(solutionRoot, StringComparison.OrdinalIgnoreCase) &&
                        p.Name.EndsWith(processName, StringComparison.OrdinalIgnoreCase))
                    .FirstOrDefault();

                if (goodProcess == null)
                {
                    ShowErrorMessage($"Could not find running process {processName} to attach to");
                    return;
                }

                goodProcess.Attach();
            }
            catch (Exception ex)
            {
                ShowErrorMessage("Failed to attach the debugger: " + ex.Message);
            }
        }
    }
}
